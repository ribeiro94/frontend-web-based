package com.example.pessoas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.pessoas.model.Pessoas;

public interface PessoasRepository extends JpaRepository<Pessoas, Long>{

}
