package com.example.pessoas.resource;


import java.util.List;



import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import com.example.pessoas.event.RecursoCriadoEvent;
import com.example.pessoas.model.Pessoas;
import com.example.pessoas.repository.PessoasRepository;
import com.example.pessoas.service.PessoasService;

@RestController
@RequestMapping("/pessoas")
public class PessoasResource {
	
	
		
	@Autowired
	private PessoasRepository pessoasRepository;
	
	@Autowired
	private PessoasService pessoasService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	public List<Pessoas> listar(){
		return pessoasRepository.findAll();
		
	}
	
	@PostMapping
	public ResponseEntity<Pessoas> criar(@RequestBody Pessoas pessoas, HttpServletResponse response) {
		Pessoas pessoasSalva = pessoasRepository.save(pessoas);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, pessoasSalva.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(pessoasSalva);
	}
	
	@GetMapping("/{id}")
	public Pessoas buscarPeloId(@PathVariable Long id) {
		return pessoasRepository.findOne(id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long id) {
		pessoasRepository.delete(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Pessoas> atualizar(@PathVariable Long id, @Valid @RequestBody Pessoas pessoas) {
		Pessoas pessoasSalva = pessoasService.atualizar(id, pessoas);
				
		return ResponseEntity.ok(pessoasSalva);
		
	}

	
	
}
