package com.example.pessoas.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.pessoas.model.Pessoas;
import com.example.pessoas.repository.PessoasRepository;

@Service
public class PessoasService {
	
	@Autowired
	private PessoasRepository pessoasRepository;

	public Pessoas atualizar(Long id, Pessoas pessoas) {
		Pessoas pessoasSalva = pessoasRepository.findOne(id);
		if (pessoasSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(pessoas, pessoasSalva, "id");
		return pessoasRepository.save(pessoasSalva);
	}
	
}
