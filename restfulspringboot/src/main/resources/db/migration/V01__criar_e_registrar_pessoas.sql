CREATE TABLE pessoas (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	data_nasc DATE,
	cpf VARCHAR(30),
	sexo VARCHAR(30),
	endereco VARCHAR(100)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

