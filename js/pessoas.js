function g1(){
const canvas = document.getElementById("barChart");
const ctx = canvas.getContext('2d');

 const data = {
    datasets: [{
        backgroundColor: [
            '#ffa1b5',
            '#4bc0c0',
            '#ff3d67',
            '#ffcd56',
            '#36a2eb'],
        data: [10, 20, 30, 40, 50],
        borderColor:	['#ffa1b5','#4bc0c0','#ff3d67', '#ffcd56','#36a2eb']
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
        '0 a 9 anos',
        '10 a 19 anos',
        'Maior que 40 anos',
        '30 a 39 anos',
        '20 a 29 anos'
    ]
};


const options = {
        title: {
                  display: true,
                  text: 'Faixa etária de idades das pessoas cadastradas',
                  position: 'top'
                  
              },
      
};

const myBarChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: options
});

}
// Grafico 2


function g2(){
    const canvas = document.getElementById("barSexo");
    const ctx = canvas.getContext('2d');
    
     const data = {
        datasets: [{
            backgroundColor: [
                '#4bc0c0',
                '#36a2eb'],
            data: [50, 50],
            borderColor:['#4bc0c0','#36a2eb']
        }],
    
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Masculino',
            'Feminino'
        ]
    };
    
    
    const options = {
            title: {
                      display: true,
                      text: 'Quantidade de pessoas do sexo Masculino e Feminino',
                      position: 'top'
                      
                  },
          
    };
    
    const myBarChart = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: options
    });
    
    }
